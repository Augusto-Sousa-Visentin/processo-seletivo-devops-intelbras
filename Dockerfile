FROM node

WORKDIR /app

COPY /web_server /app

RUN npm install

RUN ls -lha

RUN npm run test

RUN npm run build

RUN ls -lha

EXPOSE 5000

CMD [ "npm", "start" ]
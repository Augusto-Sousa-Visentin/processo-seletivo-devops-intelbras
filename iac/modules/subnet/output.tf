#outputs para referencia em outro módulo
output "subnet" {
    value = aws_subnet.intelbras-subnet-1
}

output "default_route_table" {
    value = aws_default_route_table.intelbras-route-table
}
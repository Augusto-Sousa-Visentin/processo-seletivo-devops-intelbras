#criação de subnet para isolamento de tráfego dentro da VPC
resource "aws_subnet" "intelbras-subnet-1" {
    vpc_id = var.vpc_id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags = {
        Name: "${var.prefix}-subnet-1"
    }
}
#conexão com a internet
resource "aws_internet_gateway" "intelbras-igw" {
    vpc_id = var.vpc_id
    tags = {
        Name: "${var.prefix}-igw"
    }
}
#adição de permissão à route tabl padrão
resource "aws_default_route_table" "intelbras-route-table" {
    default_route_table_id = var.default_route_table_id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.intelbras-igw.id
    }
    tags = {
        Name: "${var.prefix}-default-rtb"
    }
}
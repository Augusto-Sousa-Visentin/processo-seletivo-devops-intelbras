#abrindo porta 5000 para tráfego externo e todas as portas para egress do servidor
resource "aws_security_group" "intelbras-sg" {
    name = "intelbras-sg"
    vpc_id = var.vpc_id

    ingress{

        from_port = 5000
        to_port = 5000
        protocol = "tcp"

        cidr_blocks = ["0.0.0.0/0"]
    }

    egress{

        from_port = 0
        to_port = 0
        protocol = "-1"

        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name: "${var.prefix}-sg"
    }

}
#dados da imagem mais recente para uso
data "aws_ami" "ubuntu-24-04-lts-image" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["ubuntu/images/hvm-ssd-gp3/ubuntu-noble-24.04-amd64*"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }
}
#criação de ec2 t2.micro
resource "aws_instance" "intelbras-instance" {
    ami = data.aws_ami.ubuntu-24-04-lts-image.id
    instance_type = "t2.micro"
    subnet_id = var.subnet_id
    vpc_security_group_ids = [aws_security_group.intelbras-sg.id]
    availability_zone = var.avail_zone
    associate_public_ip_address = true
    key_name = "test2"
    user_data = file("./modules/web_server/init-script.sh")
    tags = {
        Name: "${var.prefix}-server"
    }
}

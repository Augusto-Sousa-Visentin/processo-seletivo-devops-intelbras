variable "avail_zone" {
  type    = string
  default = "sa-east-1a"
}
variable prefix {
    type    = string
    default = "dev"
}
variable subnet_cidr_block {
    type    = string
    default = "10.0.10.0/25"

}
variable vpc_cidr_block {
    type    = string
    default = "10.0.10.0/24"
}
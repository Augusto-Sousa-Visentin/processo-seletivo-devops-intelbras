#referenciando modulo de subnet
module "intelbras-subnet" {
    source = "./modules/subnet"
    subnet_cidr_block = var.subnet_cidr_block
    avail_zone = var.avail_zone
    prefix = var.prefix
    vpc_cidr_block = var.vpc_cidr_block
    vpc_id = aws_vpc.intelbras-vpc.id
    default_route_table_id = aws_vpc.intelbras-vpc.default_route_table_id
}
#referenciando modulo webserver
module "intelbras-webserver" {
    source = "./modules/web_server"
    vpc_id = aws_vpc.intelbras-vpc.id
    prefix = var.prefix
    subnet_id = module.intelbras-subnet.subnet.id
    avail_zone = var.avail_zone
}
#criação de vpc simples
resource "aws_vpc" "intelbras-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name: "${var.prefix}-vpc"
    }
}

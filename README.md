# Projeto: Deploy de serviço com CI/CD - Processo seletivo Squad Software TMR - Intelbras

**Instruções para execução do projeto:**

**Clone o repositório** \
    git clone https://gitlab.com/Augusto-Sousa-Visentin/processo-seletivo-devops-intelbras.git\
**Faça o push do mesmo para o seu repositório particular** \
    Siga os primeiros dois passos do tutorial: https://learn.liferay.com/w/liferay-cloud/getting-started/configuring-your-gitlab-repository\
**Adicione as variaveis de CI/CD via UI** \
**Siga o [tutorial](https://docs.gitlab.com/ee/ci/variables/) para adição das variáveis abaixo:** \
    1.     AWS_KEY = chave AWS para deploy do projeto \
    2.     AWS_SECRET_KEY = chave secreta da AWS para deploy do projeto \
    3.     DOCKER_USER = usuário docker para login no repositório de upload da imagem \
    4.     DOCKER_TOKEN = token docker para login \
\
**Rode a [pipeline](https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually) e acione manualmente o job "apply" no stage de deploy:**\
**Acesse o painel EC2 da conta AWS e encontre o endereço IPV4 da instancia criada seguindo o [tutorial](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiS-vOdpPSGAxVjD7kGHSZ0APwQFnoECA4QAw&url=https%3A%2F%2Fdocs.aws.amazon.com%2Fmanagedservices%2Flatest%2Fonboardingguide%2Ffind-instance-id.html%23%3A~%3Atext%3DThe%2520AMS%2520Console%2520for%2520a%2CInstance%2520ID%2520and%2520IP%2520address.&usg=AOvVaw2E3OlaxBEd2pHcAQyug4mN&opi=89978449)

Você encontrará a seguinte instancia criada:\
![alt text](image.png)

**Acesse o endereço IPV4 da instacia pelo navegador através da URL: http://<endereço_instancia>:5000** \
O resultado esperado é o mesmo que a imagem abaixo: \
![alt text](image2.png) \
**Após a execução do apply é estritamente necessário que seja executado também o job "destroy" antes do próximo deploy**